#!/bin/bash

# Fetch dynamic fields
IPADDR=$(/sbin/ifconfig eth0 | awk '/inet / { print $2 }' | sed 's/addr://')

# Set timezone to Australia/Brisbane (AEST)
ln -sf /usr/share/zoneinfo/Australia/Brisbane /etc/localtime

# Synchronise hardware clock
hwclock --systohc

# Configure locales
sed --in-place --expression 's|<U002E> IGNORE;IGNORE;IGNORE;<U002E> % FULL STOP|<U002E> <RES-1>;IGNORE;IGNORE;<U002E> % FULL STOP|' /usr/share/i18n/locales/iso14651_t1_common
sed --in-place --expression 's|#en_AU.UTF-8 UTF-8|en_AU.UTF-8 UTF-8|'   /etc/locale.gen
sed --in-place --expression 's|#en_AU ISO-8859-1|en_AU ISO-8859-1|'     /etc/locale.gen
sed --in-place --expression 's|#en_GB.UTF-8 UTF-8|en_GB.UTF-8 UTF-8|'   /etc/locale.gen
sed --in-place --expression 's|#en_GB ISO-8859-1|en_GB ISO-8859-1|'     /etc/locale.gen
sed --in-place --expression 's|#en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|'   /etc/locale.gen
sed --in-place --expression 's|#en_US ISO-8859-1|en_US ISO-8859-1|'     /etc/locale.gen
locale-gen
echo LANG=en_AU.UTF-8 > /etc/locale.conf

# Set hostname
echo $HOSTNAME > /etc/hostname
hostname -F /etc/hostname

# Configure hosts file
echo "127.0.0.1 localhost
::1 localhost
$IPADDR $FQDN $HOSTNAME" >> /etc/hosts

# Enable wheel group
sed --in-place --expression 's|# %wheel ALL=(ALL) ALL|%wheel ALL=(ALL) ALL|' /etc/sudoers

# Configure pacman
sed --in-place --expression 's|#Color|Color|'                       /etc/pacman.conf
sed --in-place --expression 's|#VerbosePkgLists|VerbosePkgLists|'   /etc/pacman.conf

# Configure package mirrors
pacman -Sy --noconfirm reflector
sed --in-place --expression 's|# --country France,Germany|--country Australia|' /etc/xdg/reflector/reflector.conf
systemctl enable --now reflector.service
systemctl enable --now reflector.timer

# Configure custom package repository
echo '
[arch-packages]
SigLevel = Optional TrustAll
Server = https://arch.isekai.dev/file/arch-packages/$arch' >> /etc/pacman.conf

# Install packages
pacman -Syu --noconfirm \
    bat \
    bind \
    colordiff \
    downgrade \
    exa \
    htop \
    nano-syntax-highlighting \
    neofetch \
    pacman-contrib \
    pkgfile \
    pkgstats \
    ripgrep \
    tree \
    tmux \
    zsh \
    zsh-autosuggestions \
    zsh-completions \
    zsh-syntax-highlighting \
    zsh-theme-powerlevel10k-git

# Create new user
useradd -m -G wheel -s /bin/zsh $USERNAME
echo "$USERNAME:$PASSWORD" | chpasswd

# Enable pkgfile
pkgfile -u
systemctl enable --now pkgfile-update.timer

# Enable pkgstats
systemctl start pkgstats.timer

# Configure nano
echo '
include "/usr/share/nano/*.nanorc"
include "/usr/share/nano-syntax-highlighting/*.nanorc"

set atblanks
set backup
set historylog
set indicator
set linenumbers
set magic
set positionlog
set smarthome
set softwrap
set stateflags
set tabsize 4
set tabstospaces
set trimblanks
set unix

set titlecolor brightblack,blue
set statuscolor brightblack,green
set numbercolor cyan
set keycolor cyan
set functioncolor green' >> /etc/nanorc

# Don't keep new mirrorlists on upgrade
mkdir /etc/pacman.d/hooks
curl -L https://gitlab.com/isekai/infrastructure/stackscripts/-/raw/main/arch-linux/etc/pacman.d/hooks/95-remove-default-mirrors.hook > /etc/pacman.d/hooks/95-remove-default-mirrors.hook

# Configure neofetch (for user)
mkdir /home/$USERNAME/.config
mkdir /home/$USERNAME/.config/neofetch
curl -L https://gitlab.com/isekai/infrastructure/stackscripts/-/raw/main/arch-linux/home/.config/neofetch/config.conf > /home/$USERNAME/.config/neofetch/config.conf
chown -R $USERNAME:$USERAME /home/$USERNAME/.config

# Configure ZSH keys (for user)
curl -L https://gitlab.com/isekai/infrastructure/stackscripts/-/raw/main/arch-linux/home/.zsh_keys > /home/$USERNAME/.zsh_keys
chown $USERNAME:$USERNAME /home/$USERNAME/.zsh_keys

# Configure ZSH aliases (for user)
curl -L https://gitlab.com/Kage-Yami/stackscripts/-/raw/main/arch-linux/home/.zsh_aliases > /home/$USERNAME/.zsh_aliases
chown $USERNAME:$USERNAME /home/$USERNAME/.zsh_aliases

# Configure ZSH (for user)
curl -L https://gitlab.com/isekai/infrastructure/stackscripts/-/raw/main/arch-linux/home/.zshrc > /home/$USERNAME/.zshrc
chown $USERNAME:$USERNAME /home/$USERNAME/.zshrc
curl -L https://gitlab.com/isekai/infrastructure/stackscripts/-/raw/main/arch-linux/home/.p10k.zsh > /home/$USERNAME/.p10k.zsh
chown $USERNAME:$USERNAME /home/$USERNAME/.p10k.zsh

# Configure SSH
mkdir /home/$USERNAME/.ssh
mv /root/.ssh/authorized_keys /home/$USERNAME/.ssh/authorized_keys
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
sed --in-place --expression 's|PermitRootLogin yes|PermitRootLogin no|' /etc/ssh/sshd_config
sed --in-place --expression 's|#PasswordAuthentication yes|PasswordAuthentication no|' /etc/ssh/sshd_config

# Lock root user
passwd -l root

# Reboot
reboot now
